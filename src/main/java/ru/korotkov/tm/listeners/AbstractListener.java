package ru.korotkov.tm.listeners;

import java.util.ResourceBundle;

public abstract class AbstractListener {

    protected final ResourceBundle bundle = ResourceBundle.getBundle("MessagesBundle");

}
