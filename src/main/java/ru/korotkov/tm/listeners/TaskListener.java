package ru.korotkov.tm.listeners;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.korotkov.tm.constant.TerminalConst;
import ru.korotkov.tm.entity.Command;
import ru.korotkov.tm.entity.Task;
import ru.korotkov.tm.exception.NotExistElementException;
import ru.korotkov.tm.observer.Listener;
import ru.korotkov.tm.service.ProjectTaskService;
import ru.korotkov.tm.service.SessionService;
import ru.korotkov.tm.service.TaskService;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TaskListener extends AbstractListener implements Listener {

    private static final Logger logger = LogManager.getLogger(TaskListener.class);

    private final TaskService taskService = TaskService.getInstance();

    private final SessionService sessionService = SessionService.getInstance();

    private final ProjectTaskService projectTaskService = ProjectTaskService.getInstance();

    @Override
    public void execute(Command command) {
        try {
            switch (command.getCommand()) {
                case TerminalConst.TASK_CREATE:
                    createTask(command.getArguments());
                    break;
                case TerminalConst.TASK_CLEAR:
                    clearTask();
                    break;
                case TerminalConst.TASK_LIST:
                    listTask(command.getArguments());
                    break;
                case TerminalConst.TASK_VIEW:
                    viewTask(command);
                    break;
                case TerminalConst.TASK_VIEW_BY_PROJECT:
                    findTaskByProjectId(command.getArguments());
                    break;
                case TerminalConst.TASK_REMOVE_FROM_PROJECT:
                    removeTaskFromProject(command.getArguments());
                    break;
                case TerminalConst.TASK_REMOVE:
                    removeTask(command);
                    break;
                case TerminalConst.TASK_UPDATE:
                    updateTask(command);
                    break;
                case TerminalConst.TASK_ADD_TO_PROJECT:
                    displayTask(addTaskToProject(command.getArguments()));
                    break;
                default:
                    break;
            }
        } catch (NotExistElementException exception) {
            System.out.println(exception.getMessage());
        }
    }

    /**
     * Add task to project
     *
     * @param arguments - arguments of command
     */
    public Task addTaskToProject(final String[] arguments) {
        final Long userId = sessionService.getUserId();
        final Long projectId = arguments.length > 0 ? Long.parseLong(arguments[0]) : null;
        final Long taskId = arguments.length > 1 ? Long.parseLong(arguments[1]) : null;
        return projectTaskService.addTaskToProject(projectId, taskId, userId);
    }

    /**
     * Create task
     *
     * @param arguments - arguments of command
     */
    public void createTask(final String[] arguments) {
        final String name = arguments.length > 0 ? arguments[0] : "";
        final String description = arguments.length > 1 ? arguments[1] : "";
        final Long userId = sessionService.getUserId();
        if (description == null) {
            taskService.create(name, userId);
        } else {
            taskService.create(name, description, userId);
        }
        logger.info(bundle.getString("taskCreate"));
    }

    /**
     * Clear tasks
     */
    public void clearTask() {
        taskService.clear(sessionService.getUserId());
        logger.info(bundle.getString("taskClear"));
    }

    /**
     * View task
     *
     * @param command - command
     */
    public void viewTask(final Command command) {
        final String option = command.getArgumentByIndex(0);
        final String param = command.getArgumentByIndex(1);
        if (option == null || param == null) {
            logger.info(bundle.getString("commandSyntaxError"));
            return;
        }
        Task task = null;
        final Long userId = sessionService.getUserId();
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                task = taskService.findByIndex(Integer.parseInt(param) - 1, userId);
                break;
            case TerminalConst.OPTION_NAME:
                task = taskService.findByName(param, userId);
                break;
            case TerminalConst.OPTION_ID:
                task = taskService.findById(Long.parseLong(param), userId);
                break;
        }
        displayTask(task);
    }

    /**
     * Display task
     *
     * @param task task
     */
    public void displayTask(Task task) {
        if (task == null) {
            logger.info(bundle.getString("notFound"));
            return;
        }

        logger.info("ID: {}", task.getId());
        logger.info("NAME: {}",task.getName());
        logger.info("DESCRIPTION: {}", task.getDescription());
        logger.info("PROJECTID: {}", task.getProjectId());
    }

    /**
     * Remove task
     *
     * @param command - command
     */
    public void removeTask(final Command command) throws NotExistElementException {
        final String option = command.getArgumentByIndex(0);
        final String param = command.getArgumentByIndex(1);
        if (option == null || param == null) {
            logger.info(bundle.getString("commandSyntaxError"));
            return;
        }
        Task task = null;
        final Long userId = sessionService.getUserId();
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                task = taskService.removeByIndex(Integer.parseInt(param) - 1, userId);
                break;
            case TerminalConst.OPTION_NAME:
                task = taskService.removeByName(param, userId);
                break;
            case TerminalConst.OPTION_ID:
                task = taskService.removeById(Long.parseLong(param), userId);
                break;
        }
        displayTask(task);
    }

    /**
     * Update task
     *
     * @param command - command
     */
    public void updateTask(Command command)  throws NotExistElementException {
        final String option = command.getArgumentByIndex(0);
        final String param = command.getArgumentByIndex(1);
        final String name = command.getArgumentByIndex(2);
        final String description = command.getArgumentByIndex(3);
        if (option == null || param == null || name == null) {
            logger.info(bundle.getString("commandSyntaxError"));
            return;
        }
        Task task = null;
        final Long userId = sessionService.getUserId();
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                if (description == null) {
                    task = taskService.updateByIndex(Integer.parseInt(param) - 1, name, userId);
                } else {
                    task = taskService.updateByIndex(Integer.parseInt(param) - 1, name, description, userId);
                }
                break;
            case TerminalConst.OPTION_ID:
                if (description == null) {
                    task = taskService.updateById(Long.parseLong(param), name, userId);
                } else {
                    task = taskService.updateById(Long.parseLong(param), name, description, userId);
                }
                break;
        }
        displayTask(task);
    }

    /**
     * List tasks
     *
     * @param arguments - arguments of command
     */
    public void listTask(final String[] arguments) {
        final String option = arguments.length > 0 ? arguments[0] : "";

        List<Task> tasks;
        final Long userId = sessionService.getUserId();
        switch (option) {
            case TerminalConst.OPTION_NAME:
                tasks = taskService.findAll(userId, Comparator.comparing(Task::getName));
                break;
            case TerminalConst.OPTION_ID:
                tasks = taskService.findAll(userId, Comparator.comparing(Task::getId));
                break;
            default:
                tasks = taskService.findAll(userId);
                break;
        }
        int index = 1;
        for (final Task task : tasks) {
            logger.info("INDEX: {} ID: {} PROJECT_ID: {} NAME: {} DESCRIPTION: {}", index++, task.getId(), task.getProjectId(), task.getName(), task.getDescription());
        }
    }

    /**
     * View tasks by projectId
     *
     * @param arguments - arguments of command
     */
    public void findTaskByProjectId(final String[] arguments) {
        final Long userId = sessionService.getUserId();
        final Long projectId = arguments.length > 0 ? Long.parseLong(arguments[0]) : null;
        for (final Task task: taskService.findTasksByProjectId(projectId, userId)) {
            displayTask(task);
        }
    }

    /**
     * Remove task from project
     *
     * @param arguments - arguments of command
     */
    public void removeTaskFromProject(final String[] arguments) {
        final Long userId = sessionService.getUserId();
        final Long taskId = arguments.length > 0 ? Long.parseLong(arguments[0]) : null;
        Task task = taskService.removeTaskFromProject(taskId, userId);
        displayTask(task);
    }

}
