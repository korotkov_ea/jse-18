package ru.korotkov.tm.listeners;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.korotkov.tm.constant.TerminalConst;
import ru.korotkov.tm.entity.Command;
import ru.korotkov.tm.entity.User;
import ru.korotkov.tm.observer.Listener;
import ru.korotkov.tm.service.SessionService;
import ru.korotkov.tm.service.UserService;

public class SystemListener extends AbstractListener implements Listener {

    private static final Logger logger = LogManager.getLogger(SystemListener.class);

    private final SessionService sessionService = SessionService.getInstance();

    private final UserService userService = UserService.getInstance();

    @Override
    public void execute(Command command) {
        sessionService.addCommand(command.getCommand());
        switch (command.getCommand()) {
            case TerminalConst.CMD_EXIT:
            case TerminalConst.USER_EXIT:
                sessionService.closeSession();
                break;
            case TerminalConst.CMD_VERSION:
                displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                displayAbout();
                break;
            case TerminalConst.CMD_HELP:
                displayHelp();
                break;
            case TerminalConst.USER_HISTORY:
                historyCommand();
                break;
            case TerminalConst.CMD_WELCOME:
                displayWelcome();
                break;
            case TerminalConst.AUTHENTICATION:
                User user = userService.authentication(command.getArgumentByIndex(0), command.getArgumentByIndex(1));
                if (user != null) {
                    sessionService.startSession(user.getId());
                    logger.info(String.format(bundle.getString("userAuthenticated"), user.getLogin()));
                }
                break;
            default:
                break;
        }
    }

    /**
     * Get userId
     *
     * @return userId - id of user
     */
    public Long getUserId() {
        return sessionService.getUserId();
    }

    /**
     * History of commands
     */
    public void historyCommand() {
        int index = 1;
        for (final String command : sessionService.findAllCommand()) {
            logger.info("INDEX: {} COMMAND: {}", index++, command);
        }
    }

    /**
     * Welcome information
     */
    public void displayWelcome() {
        logger.info(bundle.getString("welcome"));
    }

    /**
     * Display version of program
     */
    public void displayVersion() {
        logger.info(bundle.getString("version"));
    }

    /**
     * Display information about of program
     */
    public void displayAbout() {
        logger.info(bundle.getString("about"));
    }

    /**
     * Display help
     */
    public void displayHelp() {
        logger.info(bundle.getString("help"));
    }

    /**
     * Display stub
     */
    public void displayStub(final String line) {
        logger.info(String.format(bundle.getString("stub"), line));
    }

}
