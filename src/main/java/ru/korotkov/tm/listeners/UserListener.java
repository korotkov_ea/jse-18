package ru.korotkov.tm.listeners;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.korotkov.tm.constant.TerminalConst;
import ru.korotkov.tm.entity.Command;
import ru.korotkov.tm.entity.User;
import ru.korotkov.tm.enumerated.Role;
import ru.korotkov.tm.exception.DuplicateLoginException;
import ru.korotkov.tm.observer.Listener;
import ru.korotkov.tm.service.SessionService;
import ru.korotkov.tm.service.UserService;

public class UserListener extends AbstractListener implements Listener {

    private static final Logger logger = LogManager.getLogger(UserListener.class);

    private final UserService userService = UserService.getInstance();

    private final SessionService sessionService = SessionService.getInstance();

    @Override
    public void execute(Command command) {
        try {
            switch (command.getCommand()) {
                case TerminalConst.CMD_EXIT:
                    displayCloseSession();
                    break;
                case TerminalConst.USER_CREATE:
                    createUser(command);
                    break;
                case TerminalConst.USER_CLEAR:
                    clearUser();
                    break;
                case TerminalConst.USER_LIST:
                    listUser();
                    break;
                case TerminalConst.USER_VIEW:
                    viewUser(command);
                    break;
                case TerminalConst.USER_REMOVE:
                    removeUser(command);
                    break;
                case TerminalConst.USER_UPDATE:
                    updateUser(command);
                    break;
                case TerminalConst.USER_EXIT:
                    displayCloseSession();
                    break;
                case TerminalConst.REGISTER:
                    register(command);
                    break;
                case TerminalConst.PROFILE_SHOW:
                    displayProfile();
                    break;
                case TerminalConst.PROFILE_UPDATE:
                    updateProfile(command);
                    break;
                case TerminalConst.PROFILE_CHANGE_PASSWORD:
                    changePassword(command);
                    break;
                default:
                    break;
            }
        } catch (DuplicateLoginException exception) {
            System.out.println(exception.getMessage());
        }
    }

    /**
     * Create user
     *
     * @param command - command
     */
    public void createUser(final Command command) throws DuplicateLoginException {
        final String login = command.getArgumentByIndex(0);
        final String password = command.getArgumentByIndex(1);
        userService.create(login, password);
        logger.info(bundle.getString("userCreate"));
    }

    /**
     * Clear users
     */
    public void clearUser() {
        userService.clear();
        logger.info(bundle.getString("userClear"));
    }

    /**
     * View user
     *
     * @param command - command
     */
    public void viewUser(final Command command) {
        final String option = command.getArgumentByIndex(0);
        final String param = command.getArgumentByIndex(1);
        if (option == null || param == null) {
            logger.info(bundle.getString("commandSyntaxError"));
            return;
        }
        User user = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                user = userService.findByIndex(Integer.parseInt(param) - 1);
                break;
            case TerminalConst.OPTION_LOGIN:
                user = userService.findByLogin(param);
                break;
            case TerminalConst.OPTION_ID:
                user = userService.findById(Long.parseLong(param));
                break;
        }
        displayUser(user);
    }

    /**
     * Remove user
     *
     * @param command - command
     */
    public void removeUser(final Command command) {
        final String option = command.getArgumentByIndex(0);
        final String param = command.getArgumentByIndex(1);
        if (option == null || param == null) {
            logger.info(bundle.getString("commandSyntaxError"));
            return;
        }
        User user = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                user = userService.removeByIndex(Integer.parseInt(param) - 1);
                break;
            case TerminalConst.OPTION_LOGIN:
                user = userService.removeByLogin(param);
                break;
            case TerminalConst.OPTION_ID:
                user = userService.removeById(Long.parseLong(param));
                break;
        }
        displayUser(user);
    }

    /**
     * Update user
     *
     * @param command - command
     */
    public void updateUser(final Command command) {
        final String option = command.getArgumentByIndex(0);
        final String param = command.getArgumentByIndex(1);
        final String password = command.getArgumentByIndex(2);
        final String paramRole = command.getArgumentByIndex(3);
        Role role = null;
        for (final Role element : Role.values()) {
            if (element.toString().equals(paramRole)) {
                role = element;
                break;
            }
        }
        final String firstName = command.getArgumentByIndex(4);
        final String middleName = command.getArgumentByIndex(5);
        final String lastName = command.getArgumentByIndex(6);
        if (option == null || param == null) {
            logger.info(bundle.getString("commandSyntaxError"));
            return;
        }
        User user = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                user = userService.updateByIndex(Integer.parseInt(param) - 1, password, role, firstName, middleName, lastName);
                break;
            case TerminalConst.OPTION_LOGIN:
                user = userService.updateByLogin(param, password, role, firstName, middleName, lastName);
                break;
            case TerminalConst.OPTION_ID:
                user = userService.updateById(Long.parseLong(param), password, role, firstName, middleName, lastName);
                break;
        }
        displayUser(user);
    }

    /**
     * User regiser
     *
     * @param command - command
     * @return user or null
     */
    public User register(final Command command) throws DuplicateLoginException {
        final String login = command.getArgumentByIndex(0);
        final String password = command.getArgumentByIndex(1);
        User user = userService.create(login, password);
        displayUser(user);
        return user;
    }

    /**
     * User change password
     *
     * @param command - command
     */
    public User changePassword(final Command command)  {
        final Long userId = sessionService.getUserId();
        final String password = command.getArgumentByIndex(0);
        User user = userService.changePassword(password, userId);
        if (user != null) {
            logger.info(bundle.getString("userChangePassword"));
        }
        return user;
    }

    /**
     * Display user
     *
     * @param user user
     */
    public void displayUser(User user) {
        if (user == null) {
            logger.info(bundle.getString("notFound"));
            return;
        }

        logger.info("ID: {}", user.getId());
        logger.info("LOGIN: {}", user.getLogin());
        logger.info("PASSWORD (HASH MD5): {}", user.getPassword());
        logger.info("FIRSTNAME: {}", user.getFirstName());
        logger.info("MIDDLENAME: {}", user.getMiddleName());
        logger.info("LASTNAME: {}", user.getLastName());
        logger.info("ROLE: {}", user.getRole());
    }

    /**
     * Show profile
     */
    public void displayProfile() {
        final Long userId = sessionService.getUserId();
        User user = userService.findById(userId);
        if (user == null || !user.getId().equals(userId)) {
            return;
        }
        displayUser(user);
    }

    /**
     * Update user
     *
     * @param command - command
     */
    public void updateProfile(final Command command) {
        final Long userId = sessionService.getUserId();
        final String firstName = command.getArgumentByIndex(0);
        final String middleName = command.getArgumentByIndex(1);
        final String lastName = command.getArgumentByIndex(2);
        User user = userService.updateProfile(userId, firstName, middleName, lastName);
        displayUser(user);
    }

    /**
     * List users
     */
    public void listUser() {
        int index = 1;
        for (final User user : userService.findAll()) {
            logger.info("INDEX: {} ID: {} LOGIN: {} FIRSTNAME: {} LASTNAME: {}", index++, user.getId(), user.getLogin(), user.getFirstName(), user.getLastName());
        }
    }

    /**
     * Exit user
     */
    public void displayCloseSession() {
        logger.info(bundle.getString("userExit"));
    }

}
