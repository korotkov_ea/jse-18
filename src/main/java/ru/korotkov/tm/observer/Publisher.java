package ru.korotkov.tm.observer;

import ru.korotkov.tm.entity.Command;

public interface Publisher {

    public void run();

    void subscribe(Listener listener);

    void unsubscribe(Listener listener);

    void notifySubscribers(Command command);

}