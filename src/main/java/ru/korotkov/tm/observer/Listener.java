package ru.korotkov.tm.observer;

import ru.korotkov.tm.entity.Command;

public interface Listener {

    void execute(Command command);

}