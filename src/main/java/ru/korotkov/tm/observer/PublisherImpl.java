package ru.korotkov.tm.observer;

import ru.korotkov.tm.constant.TerminalConst;
import ru.korotkov.tm.entity.Command;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class PublisherImpl implements Publisher {

    private final Set<Listener> listenerSet = new HashSet<>();

    @Override
    public void notifySubscribers(Command command) {
        if (command != null) {
            for(Listener listener : listenerSet) {
                listener.execute(command);
            }
        }
    }

    @Override
    public void subscribe(Listener listener) {
        if (listener != null) {
            listenerSet.add(listener);
        }
    }

    @Override
    public void unsubscribe(Listener listener) {
        if (listener != null) {
            listenerSet.remove(listener);
        }
    }

    /**
     * Main loop of program
     */
    public void run() {
        Scanner scanner = new Scanner(System.in);
        String command;
        while (scanner.hasNextLine()) {
            command = scanner.nextLine();
            if (!process(command)) {
                break;
            }
        }
    }

    /**
     * Process of input parameter
     *
     * @param line - command for execute
     * @return true - wait next parameter, false - exit programm
     */
    public boolean process(final String line) {
        if (line == null || line.isEmpty()) {
            return true;
        }

        final String[] parts = line.split(TerminalConst.SPLIT);
        final String command = parts[0];
        final String[] arguments = Arrays.copyOfRange(parts, 1, parts.length);

        notifySubscribers(new Command(command, arguments));

        return !TerminalConst.CMD_EXIT.equals(line);
    }

}
