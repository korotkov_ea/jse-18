package ru.korotkov.tm;

import ru.korotkov.tm.constant.TerminalConst;
import ru.korotkov.tm.listeners.*;
import ru.korotkov.tm.entity.Command;
import ru.korotkov.tm.observer.Publisher;
import ru.korotkov.tm.observer.PublisherImpl;

/**
 * Task-manager application
 *
 * @author Evgeniy Korotkov
 */
public class Application {

    /**
     * Entry point of programm
     */
    public static void main(final String[] args) {
        Publisher publisher = new PublisherImpl();
        publisher.subscribe(new SystemListener());
        publisher.subscribe(new ProjectListener());
        publisher.subscribe(new TaskListener());
        publisher.subscribe(new UserListener());
        publisher.notifySubscribers(new Command(TerminalConst.CMD_WELCOME, null));
        publisher.run();
    }

}